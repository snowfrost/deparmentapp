DROP TABLE department if EXISTS ;
CREATE TABLE department ( id   INT  NOT NULL, name VARCHAR(1000) NOT NULL, PRIMARY KEY (id) );
DROP TABLE employee if EXISTS ;
CREATE TABLE employee ( id INT  NOT NULL, name VARCHAR(1000) NOT NULL, department INT, birthday   DATE DEFAULT '2002-02-15', pay        INT, PRIMARY KEY (id) );
INSERT INTO department (id, name) VALUES (1, 'Отдел разработки');
INSERT INTO department (id, name) VALUES (2, 'Отдел поддержки');
INSERT INTO department (id, name) VALUES (3, 'Отдел маркетинга');
INSERT INTO employee (id, name, department, birthday, pay) VALUES (1, 'Иванов Иван', 1, '1988-09-21', 2000);
