package com.karpin.departmentapp.repository;

import com.karpin.departmentapp.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository

public interface EmployeeRepository  extends JpaRepository<Employee, Integer>{

    Employee findById(int id) throws DataAccessException;
}
