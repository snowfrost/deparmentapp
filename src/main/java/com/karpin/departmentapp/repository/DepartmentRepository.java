package com.karpin.departmentapp.repository;

import com.karpin.departmentapp.entity.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

@Repository
public interface DepartmentRepository extends CrudRepository<Department, Integer> {



}
