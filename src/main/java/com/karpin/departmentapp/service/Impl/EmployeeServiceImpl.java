package com.karpin.departmentapp.service.Impl;

import com.karpin.departmentapp.entity.Employee;
import com.karpin.departmentapp.repository.EmployeeRepository;
import com.karpin.departmentapp.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public Employee findById(int id) {
        return this.employeeRepository.findById(id);
    }
}
