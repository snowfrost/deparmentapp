package com.karpin.departmentapp.service;

import com.karpin.departmentapp.entity.Employee;


public interface EmployeeService {

    Employee findById(int id);
}
